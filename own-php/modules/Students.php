<?php

    class Students extends Module {
        
        public $name;
        
        private $database;
        private $core;
        
        public function __construct($database) {
            $this->name = "Студенты";
            
            $this->database = $database;
            
            $this->core = new Core();
        }
        
        public function filterArray($array){
            $result['name'] = $array['name'];
            $result['surname'] = $array['surname'];
            $result['lastName'] = $array['lastName'];
            switch ($array['gender']) {
                case 'Мужской':
                    $result['sex'] = 'M';
                    break;
                case 'Женский':
                    $result['sex'] = 'F';
                    break;
            }
            if (isset($array['password']))
            {
                if ($array['password'] == $array['password1'])
                {
                    $result['password'] = md5($array['password']);
                }
            }
            return $result;
        }
        
        public function add ()
        {
            $file = filter_input(INPUT_GET, 'file');
            if ($file = 'xls') 
            {
                $file = $_FILES['xls'];
                $download = $this->core->uploadFile($file, ROOTPATH, '/temp/');
                $data = new SpreadsheetReader(ROOTPATH.$download);
                foreach ($data as $row)
                {
                    $array['name'] = $row[0];
                    $array['surname'] = $row[1];
                    $array['lastName'] = $row[2];
                    if ($row[3] == 'Мужской')
                    {
                        $array['sex'] = 'M';
                    }
                    if ($row[3] == 'Женский')
                    {
                        $array['sex'] = 'F';
                    }
                    $array['password'] = md5($row[4]);
                    $this->database->insert($array, 'students');
                }
                    unlink(ROOTPATH.'/temp/'.$file['name']);
            } else {
                return $this->database->insert($this->filterArray($_POST), 'students');
            }
        }
        
        public function get (){
            $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
            if (isset($id)){
                return $result = $this->database->row("SELECT * FROM students WHERE id='".$id."'");
            } else {
                return false;
            }
        }
        
        public function show ($order){
            if (isset($order))
            {
                $result = $this->database->get("SELECT * FROM students ORDER BY ".$order."");
            } else {
                $result = $this->database->get("SELECT * FROM students ORDER BY id");
            }
            return $result;
        }
        
        public function update (){
            $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
            return $this->database->update($this->filterArray($_POST), 'students', $id);
        }
        
        public function delete ($id){
            return $this->database->query('DELETE FROM students WHERE id='.$id.'', '');
        }
        
        public function process (){
            switch ($_POST['process'])
            {
                case 'Удалить':
                    foreach ($_POST as $key => $value)
                    {
                        if ($value == 'on') {
                            $this->delete($key);
                        }
                    }
                    break;
                case 'Добавить в группу':
                    break;
            }
        }
        
    }

?>