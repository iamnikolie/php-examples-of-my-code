<?php


abstract class Module {

    private $status;
    
    public $name;
    public function getStatus(){
        return $this->status;
    }
    
    abstract function __construct($database);
    abstract function filterArray ($array);
    abstract function get ();
    abstract function show ($order);
    abstract function add ();
    abstract function update ();
    abstract function process ();
    abstract function delete ($id);
    
}