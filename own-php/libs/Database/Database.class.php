<?php

class Database extends Core
{
    static $hostname = '';
    static $username = '';
    static $password = '';
    
    static $dbname = '';
    
    static $linkresource = '';
    
    function __construct ($host, $user, $pass, $db)
    {
        $this->hostname = $host;
        $this->username = $user;
        $this->password = $pass;
        $this->dbname = $db;
    }
    
    function __destruct ()
    {
        mysql_close();
    }
    
    public function connect ()
    {
        $res = mysql_connect($this->hostname, $this->username, $this->password);
        if ($res == true)
        {
            $res1 = mysql_select_db($this->dbname);
            if ($res1 == true)
            {
                $this->linkresource = $res;
                return true;
            }
            else
            {
                $this->error(mysql_error());
            }
        }
        else
        {
            $this->error(mysql_error());
        }
    }
    
    public function query ($query, $striptags)
    {
        if ($striptags == TRUE)
        {
            strip_tags($query);
        }
        $query = iconv('utf-8', 'windows-1251', $query);
        $q = mysql_query("$query");
        if ($q == TRUE)
        {
            return $q;
        }
        else 
        {
            echo $query.'<br>';
            $this->error(mysql_error());
        }
    }
    
    public function insert ($array, $tablename)
    {
        foreach ($array as $key => $value)
        {
            $cols[$key] = $key;
            $values[$key] = '\''.$value.'\'';
        }
        if (sizeof($cols) == sizeof($values))
        {
            $c = implode(', ', $cols);
            $v = implode(', ', $values);
            $this->query('INSERT INTO '.$tablename.' ('.$c.') VALUES ('.$v.')', '');
        }
        else
        {
            $this->error('System error! Wrong array of cols and values! Please, check it with you web-master!');
        }
    }
    
    public function update ($array, $tablename, $id)
    {
        foreach ($array as $key => $value)
        {
            $cols[$key] = $key;
            $values[$key] = '\''.$value.'\'';
            $this->query('UPDATE '.$tablename.' SET '.$key.'=\''.$value.'\' WHERE id='.$id.'', '');
        }
    }
    
    public function get ($query)
    {
        $t = $this->query($query, FALSE);
        $i = 0;
        while ($r = mysql_fetch_assoc($t))
        {
            $res[$i] = $r;
            $i++; 
        }
        for ($i = 0; $i < sizeof($res); $i++)
        {
            foreach ($res[$i] as $key => $value)
            {
                $value = iconv('windows-1251', 'utf-8', $value);
                $res[$i][$key] = $value;
            }
        }
        return $res;
    }
    
    public function row ($query)
    {
        $t = $this->query($query, FALSE);
        $i = 0;
        while ($r = mysql_fetch_assoc($t))
        {
            $res[$i] = $r;
            $i++; 
        }
        for ($i = 0; $i < sizeof($res); $i++)
        {
            foreach ($res[$i] as $key => $value)
            {
                $value = iconv('windows-1251', 'utf-8', $value);
                $res[$i][$key] = $value;
            }
        }
        return $res[0];
    }
    
}


?>