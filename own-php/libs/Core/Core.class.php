<?php

    class Core {
        
        private function sendHeader($message)
        {
            header($message, true);
            return void;
        }
        
        public function __construct() {
            
        }
        
        public function translitString($st)
        {
            $st=strtr($st,"абвгдеёзийклмнопрстуфхъыэ_",
            "abvgdeeziyklmnoprstufh'iei");
            $st=strtr($st,"АБВГДЕЁЗИЙКЛМНОПРСТУФХЪЫЭ_",
            "ABVGDEEZIYKLMNOPRSTUFH'IEI");
            $st=strtr($st, array(
                        "ж"=>"zh", "ц"=>"ts", "ч"=>"ch", "ш"=>"sh", 
                        "щ"=>"shch","ь"=>"", "ю"=>"yu", "я"=>"ya",
                        "Ж"=>"ZH", "Ц"=>"TS", "Ч"=>"CH", "Ш"=>"SH", 
                        "Щ"=>"SHCH","Ь"=>"", "Ю"=>"YU", "Я"=>"YA",
                        "ї"=>"i", "Ї"=>"Yi", "є"=>"ie", "Є"=>"Ye", " "=>"_"
                        )
             );
            return $st;
        }
        
        public function randomString($lenght)
        {
            $characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $randstring = "";
            for ($i = 0; $i < $lenght; $i++) {
                $randstring = $characters[rand(0, strlen($characters))];
            }
            return $randstring;
        }
        
        public function error ($message)
        {
            echo $message;
            die();
        }
        
        final function charset ($charset)
        {
            $this->sendHeader("Content-Type: text/html; charset=".$charset);
        }
        
        public function refferer ()
        {
            header ("Location: ".$_SERVER['HTTP_REFERER']);
        }
        
        public function redirect ($url)
        {
            header ("Location: ".$url);
        }
        
        public function refresh($message, $time)
        {
            echo $message;
            $url = $_SERVER['HTTP_REFFERER'];
            if (!empty($url))
            {  
                header("refresh:".$time.";url=".$url);
            } else {
                header("refresh:".$time.";url=".SITEURL."/index.php");
            }
        }
        
        public function refreshURL($message, $time, $url)
        {
            echo $message;
            if (!empty($url))
            {  
                header("refresh:".$time.";url=".$url);
            } else {
                header("refresh:".$time.";url=index.php");
            }
        }
        
        public function readDIR ($dir) 
        {
            if (is_dir($dir)) {
                if ($dh = opendir($dir)) {
                    $i = 0;
                    while (($file = readdir($dh)) !== false) {
                        if (($file != '..') && ($file != '.') && ($file != 'Module.php'))
                        {
                            $result[$i] = $file;
                            $i++;
                        }
                    }
                    closedir($dh);
                }
            }
            return $result;
        }
        
        public function connectModules ()
        {
            $modules = $this->readDIR('modules');
            for ($i = 0; $i < sizeof($modules); $i++){
                include 'modules/'.$modules[$i];
            }
        }
        
        public function uploadFile ($file, $root, $path)
        {
            if (isset($file))
                {
                    if (($file['size'] > 1) && ($file['size'] < 10*1024*1024)){
                        if (is_uploaded_file($file['tmp_name']))
                        {
                            move_uploaded_file($file['tmp_name'], $root.$path.$file["name"]);
                        }
                    } else {
                        $this->status = 'ERROR_UPD_FILE';
			die('EROR');
                    }
                }
            return $path.$file['name'];
        }
        
        public function deleteFile ($file, $root, $path)
        {
            return unlink($root.$path.$file);
        }
        
    }

?>

