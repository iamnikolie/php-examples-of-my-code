<?php

    class Authorization extends Core{
        
        public $database;
        public $usersTableName;
        
        private $status;
        
        public function __construct(Database $db, $usersTableName) {
            $this->database = $db;
            $this->usersTableName = $usersTableName;
        }
        
        private function checkAuthority($login, $password)
        {
            $passwordHash = md5($password);
            $check = $this->database->row("SELECT * FROM ".$this->usersTableName." WHERE login='".$login."' AND password='".$passwordHash."'");
            if (!empty($check['id'])){
                return true;
            }
            else {
                return false;
            }
        }
        
        private function startSession($login)
        {
            $hash = md5($this->randomString(26));
                
            $q[0] = 'CREATE TABLE IF NOT EXISTS tmp_sessions '
                . '(id INT AUTO_INCREMENT PRIMARY KEY, '
                . 'login VARCHAR(255) UNIQUE NOT NULL, '
                . 'hash VARCHAR(255) UNIQUE NOT NULL, '
                . 'start TIMESTAMP DEFAULT NOW(), '
                . 'expires INT)';
                
            $q[1] = "INSERT INTO tmp_sessions (login, hash, expires)"
                . "VALUES"
                . "('".$login."', '".$hash."', '12')";
            
            if ($this->checkSession() == false){
                $q[2] =  "SELECT id FROM tmp_sessions WHERE login='"
                    .$login
                    ."'";
                $t = $this->database->row($q[2]);
                if ($t['id']) $this->database->query("DELETE FROM tmp_sessions WHERE id='"
                    .$t['id']
                    ."'", '');

                for ($i = 0; $i < sizeof($q); $i++)
                {
                    $this->database->query($q[$i], '');
                }

                setcookie('_login', $login, 0, '/');
                setcookie('_hash', $hash, 0, '/');
            }
            else {
                $this->status = "LOGINED";
            }
            
            return 0;
        }
        
        private function checkExpiresTime($session)
        {
            $q = "SELECT hash FROM tmp_sessions  WHERE start > (NOW() - INTERVAL "
                    .$session['expires']
                    ." HOUR) AND start < NOW() AND login='".$session['login']."'";
            $res = $this->database->row($q);

            if (!empty($res['hash'])) {
                if ($res['hash'] == $session['hash'])
                {
                    return true;
                } else {
                    return false;
                }
            }     
        }
        
        private function checkSession()
        {
            $hash = filter_input(INPUT_COOKIE, '_hash');
                $session = $this->database->row("SELECT * FROM tmp_sessions WHERE hash='".$hash."'");
                if (!empty($session))
                {
                    if ($this->checkExpiresTime($session) == true)
                    {
                        return true;
                    }
                    else {
                        if (!empty($_SESSION))
                        {
                            $this->database->query("DELETE * FROM tmp_sessions WHERE hash='".$hash."'", '');
                            $this->status = "TIME_EXPIRED";
                            return false;
                        }
                    }
                } else {
                    $this->status = "AUTH_DENIDED";
                    return false;
                }
            
        }
        
        public function getStatus()
        {
            return $this->status;
        }
        
        public function logIn($login, $password)
        {
            if ($this->checkAuthority($login, $password) == true) {
                $this->startSession($login);
                $this->status = "LOGINED";
            } else {
                $this->status = "NO_USER_PASS";
            }
        }
       
        public function process(){
            if ($this->checkSession()){
                return void;
            } else {
                $this->redirect(SITEURL.'/login.php');
            }
        }
        
        public function access()
        {
            if ($this->checkSession()){
                return true;
            } else {
                return false;
            }
        }
        
        public function logOut($hash)
        {
            $this->database->query("DELETE FROM tmp_sessions WHERE hash='".$hash."'", '');
                
                setcookie('_login', $login, time()-3600, '/');
                setcookie('_hash', $hash, time()-3600, '/');

            $this->refresh("Вы вышли из системы. Подождите 3 секунды, вы будете перенаправлены", 3);
        }

        public function deleteOldTmpSessions ()
        {
            $q = "SELECT * FROM tmp_sessions";
            $res = $this->database->get($q);
            for ($i = 0; $i < sizeof($res); $i++)
            {
                $q = "SELECT id FROM tmp_sessions  WHERE start > (NOW() - INTERVAL "
                    .$res[$i]['expires']
                    ." HOUR) AND start < NOW() AND login='".$res[$i]['login']."'";
                $row = $this->database->row($q);
                if (!$row['id']) {
                    $q = "DELETE FROM tmp_sessions WHERE id=".$res[$i]['id'];
                    $this->database->query($q, '');
                }
            }
        }
        
        
    }

?>