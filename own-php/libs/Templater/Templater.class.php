<?php

    class Templater extends Core 
    {
        
        public $template;
        
        public function __construct()
        {
            $this->template = new Smarty();
        }
        
        public function getGETStream ()
        {
            foreach ($_GET as $key => $value )
            {
                $this->template->assign('get'.$key, $value);
            }
            return 0;
        }
        
        public function getPOSTStream ()
        {
            foreach ($_POST as $key => $value )
            {
                $this->template->assign('post'.$key, $value);
            }
            return 0;
        }
        
        public function getConfigures (array $configuresArray)
        {
            foreach ($configuresArray as $key => $value )
            {
                $this->template->assign($key, $value);
            }
            return 0;
        }
        
        public function assignFileName ($file)
        {
            $filename = basename($file, '.php');
            $this->template->assign('filename', $filename);
            return 0;
        }
        
        public function assignPageName ($pagename)
        {
            $this->template->assign('pagename', $pagename);
            return 0;
        }
        
        public function assignDate ()
        {
            $date['y'] = date("Y");
            $date['m'] = date("m");
            $date['d'] = date("d");
            $date['h'] = date("H");
            $date['i'] = date("i");
            $date['s'] = date("s");
            foreach ($date as $key => $value)
            {
                $this->template->assign('date'.$key, $value);
            }
            $intervalMonth = filter_input(INPUT_COOKIE, '_intervalMonth');
            $intervalYear = filter_input(INPUT_COOKIE, '_intervalYear');
           
            if (($intervalMonth > 0) && ($intervalYear > 0))
            {
                $this->template->assign('countOfDays', cal_days_in_month(CAL_GREGORIAN, $intervalMonth, $intervalYear));
                $this->template->assign('nowYear', $intervalYear);
                $this->template->assign('nowMonth', $intervalMonth);
            } else {
                $this->template->assign('countOfDays', cal_days_in_month(CAL_GREGORIAN, date('n'), date('Y')));
                $this->template->assign('nowYear', date('Y'));
                $this->template->assign('nowMonth', date('n'));
            }
            return true;
        }
        
        public function assign ($variable, $value)
        {
            return $this->template->assign($variable, $value);
        }
        
        public function display ($template)
        {
            $this->assignDate();
            return $this->template->display($template);
        }
                
        
    }


?>
