<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use DB;
use Auth;
use Redirect;
use Schema;

use App\Image;
use App\Object;
use App\OptionToObject;
use App\Type;
use App\Operation;
use App\Option;
use App\User;
use App\District;
use App\Location;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ObjectsController extends Controller
{
    /**
     * Collect array of Options to Objectz
     *
     */

    private function createArrayOptions (array $array, $objectId) {

        $data = array();
        $i = 0;
        foreach ($array as $key => $value) {
            $item = explode ('_', $key);
            if ($item[0] == 'option') {
                $data[$i]['option_id'] = $item[1];
                $data[$i]['object_id'] = $objectId;
                $data[$i]['value'] = $array[$key];
                $i++;
            }
        }
        return $data;

    }

    private function addImagesToObjectGallery (array $array, $objectId){

        foreach ($array as $key => $value) {
            $count = (int)Image::where('object_id', '=', $objectId)->count();
            $item = explode ('_', $key);
            if ($item[0] == 'gallery') {
                $data['object_id'] = $objectId;
                $data['position'] = $count+1;
                $data['image'] = $value;

                Image::create($data);
            }
        }

    }

    private function addOptionsToObject (array $data){

        for ($i = 0; $i < sizeof($data); $i++){
            OptionToObject::Create($data[$i]);
        }

    }

    private function truncateOptionsOfObject ($id) {

        OptionToObject::where('object_id', '=', $id)->delete();

    }

    /**
     * Display a listing of the resource.
     *
     */

    public function index()
    {

        $data = Object::orderBy('name', 'DESC')->paginate(15);
        $counter = Object::count();
        $columns = Schema::getColumnListing('objects');

        $values = array();
        foreach ($columns as $column) {
            $values[$column] = '';
        }

        $data = [
            'items' => $data,
            'counter' => $counter,
            'values' => $values,
            'title' => 'Агентство недвижимости Realvsesvit'
        ];
        return view('admin.objects.index', $data);
    }

    public function add () {

        $userid = User::where('email', '=', Auth::user()->email)->first();

        $options = Option::orderBy('name')->get();
        $types = Type::get();
        $operations = Operation::get();
        $columns = Schema::getColumnListing('objects');
        $locations = Location::orderBy('name', 'asc')->get();
        $districts = District::get();

        $values = array();
        foreach ($columns as $column) {
            $values[$column] = '';
        }

        $optionsToTypes = DB::table('map_optionstotypes')
            ->join('types', 'types.id', '=', 'map_optionstotypes.type_id')
            ->join('options', 'options.id', '=', 'map_optionstotypes.option_id')
            ->orderBy('options.name')
            ->get(['option_id', 'types.name']);

        $data = [
            'types' => $types,
            'locations' => $locations,
            'districts' => $districts,
            'operations' => $operations,
            'options' => $options,
            'optionstotypes' => $optionsToTypes,
            'values' => $values,
            'userid' => $userid->id,
            'title' => 'Агентство недвижимости Realvsesvit'
        ];

        return view('admin.objects.add', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(array $data)
    {
        $data['type_id'] = (int)DB::table('types')
            ->where('name', $data['type'])
            ->pluck('id');

        $data['operation_id'] = (int)DB::table('operations')
            ->where('name', $data['operation'])
            ->pluck('id');

        $data['manager_id'] = (int)DB::table('managers')
            ->where('user_id', '=', $data['user_id'])
            ->pluck('id');

        return Object::create([
            'name' => $data['name'],
            'manager_id' => $data['manager_id'],
            'type_id' => $data['type_id'],
            'operation_id' => $data['operation_id'],
            'location' => $data['location'],
            'subway' => $data['subway'],
            'district' => $data['district'],
            'street' => $data['street'],
            'building' => $data['building'],
            'apartments' => $data['apartments'],
            'price' => $data['price'],
            'image' => $data['image'],
            'description' => $data['description']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:objects',
            'type' => 'required',
            'operation' => 'required',
            'location' => 'required',
            'price' => 'required',
            'district' => 'required',
            'image' => 'required',
            'price' => 'required|integer'
        ];

        $validator = $this->validate($request, $rules);

        $object = $this->create($request->all());

        $this->addOptionsToObject($this->createArrayOptions($request->all(), $object->id));
        $this->addImagesToObjectGallery($request->all(), $object->id);

        return Redirect::back();

    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $values = Object::find($id);

        if ($values->id)
        {
            $gallery = Image::where('object_id', '=', $id)->get();

            $userid = User::where('email', '=', Auth::user()->email)->first();

            $options = Option::orderBy('name')->get();
            $types = Type::get();
            $operations = Operation::get();

            $locations = Location::orderBy('name', 'asc')->get();
            $districts = District::get();

            $optionsToTypes = DB::table('map_optionstotypes')
                ->join('types', 'types.id', '=', 'map_optionstotypes.type_id')
                ->join('options', 'options.id', '=', 'map_optionstotypes.option_id')
                ->orderBy('options.name')
                ->get(['option_id', 'types.name']);

            $optionToObject = OptionToObject::where('object_id', '=', $id)->get();

            $data = [
                'types' => $types,
                'locations' => $locations,
                'districts' => $districts,
                'operations' => $operations,
                'options' => $options,
                'optionstotypes' => $optionsToTypes,
                'optionstoobject' => $optionToObject,
                'values' => $values,
                'userid' => $userid->id,
                'gallery' => $gallery,
                'title' => 'Агентство недвижимости Realvsesvit'
            ];

            return view('admin.objects.edit', $data);
        } else {
            return Redirect::back();
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Object::find($id);

        if ($item->id)
        {
            if ($item['name'] != $request->name){
                $rules['name'] = 'required|unique:objects';
            } else {
                $rules['name'] = 'required';
            }
            $rules = [
                'type' => 'required',
                'operation' => 'required',
                'location' => 'required',
                'price' => 'required',
                'district' => 'required',
                'price' => 'required|integer'
            ];

            $validator = $this->validate($request, $rules);

            $this->truncateOptionsOfObject($item->id);
            $this->addImagesToObjectGallery($request->all(), $item->id);

            $this->addOptionsToObject($this->createArrayOptions($request->all(), $item->id));
            $item->update($request->all());
            $item->image->destroy(['original']);
            return Redirect::back();
        } else {
            return Redirect::back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Object::find($id);
        if ($item->id)
        {
            Image::where('object_id', '=', $id)->delete();
            OptionToObject::where('object_id', '=', $id)->delete();
            Object::destroy($id);
            return Redirect::back();
        } else {
            return Redirect::back();
        }
    }

    public function imageDestroy($id)
    {
        $item = Object::find($id);
        if (isset($item->id))
        {
            $item->image = STAPLER_NULL;
            $item->save();
            $item->image->destroy(['original']);
            return Redirect::back();
        } else {
            return Redirect::back();
        }
    }
}
